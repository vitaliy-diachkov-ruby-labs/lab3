# Read sentence from file
input = File.read("input.txt").split

# Clone initial array
output = input.map(&:clone)

# Randomly shuffle words
output_string = output.shuffle.join(" ")

# Save results to output file
File.write("output.txt", output_string, mode: "w+")
