# Each line in file represents a single number. Read them all from file, split
# one by one and convert to integers
input = File.read("input.txt").split.map(&:to_i)

# Clone inputed array to new object
output = input.map(&:clone)

# Get indexes of min and max element
min_index = output.index(output.min)
max_index = output.index(output.max)

# Swap min and max elements in array
output[min_index], output[max_index] = output[max_index], output[min_index]

# Write new array to output file
output_file = File.open("output.txt", "w+")
output.each do |number|
  output_file.write("#{number}\n")
end
output_file.close
