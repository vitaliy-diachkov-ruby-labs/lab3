print "Enter directory path: "
path = gets.chomp

if !path.end_with? "/"
  path = path + "/"
end

size = Dir[path + "*"].select { |f| File.file?(f) }.sum { |f| File.size(f) }
puts "Total size: #{size} bytes"
